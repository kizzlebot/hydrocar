import glob
import time

class w1_temp:
  def __init__(self, debug = False):
    self.device_path = glob.glob('/sys/devices/w1_bus_master1/28-*/w1_slave')[0]

  def read_temp_raw(self):
    f = open(self.device_path, 'r')
    lines = f.readlines()
    f.close()
    return lines

    # def read_temp_raw(self):
    #   catdata = subprocess.Popen(['cat',device_file], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    #   out,err = catdata.communicate()
    #   out_decode = out.decode('utf-8')
    #   lines = out_decode.split('\n')
    #   return lines

  def read_temp(self):
    lines = self.read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
      time.sleep(0.2)
      lines = self.read_temp_raw()

    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
      temp_string = lines[1][equals_pos+2:]
      return float(temp_string) / 1000.0
