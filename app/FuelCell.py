from enum import Enum
import platform
import os



if platform.linux_distribution() != ('','','') and platform.architecture() == ('32bit', 'ELF'):
  from bbio import GPIO0_8, GPIO2_14, GPIO2_12, GPIO2_2, OUTPUT, INPUT, HIGH, LOW, AIN5, AIN3, pinMode, pinState, digitalWrite, digitalRead, delay, analogRead
  from devices.ina219 import INA219
  from devices.w1_temp import w1_temp
else:
  from tests.bbio_mock import *
  from tests.ina219_mock import INA219
  from tests.w1_temp_mock import w1_temp

from fuelcell.fuelcell_state import State
from fuelcell.io_processor import IO_Processor



class FuelCell(IO_Processor):
  gpio_out_pins = {'inlet':GPIO0_8, 'purge':GPIO2_14, 'fan':GPIO2_12, 'powerSourceSelect':GPIO2_2 }
  gpio_in_pins = {}
  analog_in_pins = {'cell1':AIN5, 'cell2':AIN3 }


  pin_values = {}
  
  PURGE_THRESHOLD = 6
  COOLDOWN_TEMP = 45
  COOLDOWN_TEMP_MIN = 35
  PURGE_DELAY = 500
  CELL_DIFFERENTIAL_THRESHOLD = 0.05



  def __init__(self, debug=True):
    super(FuelCell, self).__init__(self.gpio_out_pins, self.gpio_in_pins, self.analog_in_pins, debug)
    self.debug = debug
    self.batteryVoltage = INA219(0x40, debug)
    self.stackVoltage = INA219(0x41, debug)
    self.w1_temp = w1_temp(debug)

    

  def reset(self):
    self.debug_print('reset')
    self.set_control_pins({'fan':False, 'inlet': False, 'powerSourceSelect':True, 'purge':False})
    self.get_fuelcell_temp()
    self.get_battery_voltage()

    self.set_state('starting', State.Starting)

  def starting(self):
    """Waits until stack voltage is greater than 6 then changes state"""
    self.debug_print('starting')
    self.set_control_pins({'fan':False, 'inlet': True, 'powerSourceSelect':True, 'purge':False})
    self.get_fuelcell_temp()
    self.get_battery_voltage()

    if ( self.get_stack_voltage() > 6 ):
      self.set_state('running', State.Running)

  def running(self):
    self.set_control_pins({'fan':False, 'inlet': True, 'powerSourceSelect':False, 'purge':False})
    self.debug_print('running')
    # If voltage less than 6, purge
    if self.get_stack_voltage() < self.PURGE_THRESHOLD:
			self.purge()

		# If temperature too high, goto cooldown
    if self.get_fuelcell_temp() > self.COOLDOWN_TEMP:      
		  self.set_state('cooldown', State.Cooldown)


  def cooldown(self):
    self.set_control_pins({'fan':True, 'inlet': False, 'powerSourceSelect':True, 'purge':False})
    self.debug_print('cooldown')
    self.get_fuelcell_temp()
    temp = (self.previous_pin_values['fuelCellTemp'] + self.pin_values['fuelCellTemp']) / 2
    if temp < self.COOLDOWN_TEMP_MIN:
    	self.set_state('starting', State.Starting)


  def shutdown(self):
    self.set_control_pins({'fan':False, 'inlet': False, 'powerSourceSelect':True, 'purge':False})
    self.set_state('shutdown', State.Shutdown)


  def execute_state(self, callback=None):
    """ Checks the cell differential then executes appropriate state method"""
    func_map = {
      0: self.reset,
      1: self.starting,
      2: self.running,
      3: self.cooldown,
      4: self.shutdown
    }

    func_map[self.current_state]()
    if self.get_cell_differential() > self.CELL_DIFFERENTIAL_THRESHOLD:
    	self.purge()

    # self.debug_print(self.pin_values)
    if callback != None:
      calback(self.pin_values)
    return self.pin_values


  def get_config(self):
    res = {}
    res['purge'] = self.PURGE_THRESHOLD
    res['cooldown'] = self.COOLDOWN_TEMP
    res['cooldown_min'] = self.COOLDOWN_TEMP_MIN
    res['delay'] = self.PURGE_DELAY
    return res

  def debug_print(self, txt):
    if self.debug:
      print ("\033[91m {}\033[00m".format(txt))

