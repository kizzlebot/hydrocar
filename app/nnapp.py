from bottle import Bottle, route, request, abort, template, static_file


from gevent.pywsgi import WSGIServer
from geventwebsocket.handler import WebSocketHandler
from geventwebsocket.websocket import WebSocketError

import os
import logging
import json
import platform

#NOTE: For different chips, you can import modules which describe the PRU hardware. The hardware-specific configuration automatically propogates throughout the app(both backend and frontend)
import am335x.pruss as pruss

#GEvent loops
import gevent
import evt
from bottle_jade import JadePlugin

from FuelCell import FuelCell



logging.basicConfig(format='\033[91m%(levelname)s\033[00m:%(message)s', level=logging.INFO)


#Initialize the app object and necessary constants
app = Bottle()
APP_PATH = os.path.dirname(os.path.abspath(__file__))

templates = os.path.join(APP_PATH, 'views')
jade = app.install(JadePlugin(template_folder=templates))




@app.route('/')
def application():
    logging.info('jade rendered: index.jade')
    print('jade rendered: index.jade')
    return jade.render('index.jade')


@app.route('/static/<filename:path>')
def static(filename):
    return static_file(filename, root=os.path.join(APP_PATH, 'static'))

@app.route('/imgs/<filename:path>')
def static_imgs(filename):
    return static_file(filename, root=os.path.join(APP_PATH, 'static', 'imgs'))


@app.route('/websocket')
def handle_websocket():
    wsock = request.environ.get('wsgi.websocket')
    if not wsock:
        abort(400, 'Expected WebSocket request.')

    #Start parallel event loops for monitoring the hardware and sending data
    monitor_prus_loop = gevent.spawn(evt.monitor_prus,prus)
    monitor_fuelcell_loop = gevent.spawn(evt.monitor_fuelcell, fuelcell)
    send_message_loop = gevent.spawn(evt.send_message, wsock)

    #Start the primary event loop for receiving commands from the front-end UI
    while True:
        try:
            print('Waiting to Receive')

            m = wsock.receive()
            print('Received')
            if m is not None:
                #NOTE: Every valid message sent will have a pruState hash and a type string
                message = json.loads(m)
                response = {}

                print message
                if message['action'] == "connect":
                    #On a connection, send a hash containing the PRUs
                    print("CONNECTION ESTABLISHED")

                    response['type'] = 'connection'
                    response['status'] = 'success'

                    response['availablePRUStates'] = {}
                    for k,v in prus.iteritems():
                      response['availablePRUStates'][k] = v.get_state()
                    evt.queue.put(response)

                # elif message['action'] == 'control_params':
                #     logging.debug('control_params')
                #     if message['type'] == 'purge':
                #         print('message[type] = purge')
                #         fuelcell.PURGE_THRESHOLD = float(message['data'])
                #     elif message['type'] == 'delay':
                #         print('message[type] = delay')
                #         fuelcell.PURGE_DELAY = float(message['data'])
                #     elif message['type'] == 'cooldown':
                #         print('message[type] = cooldown')
                #         fuelcell.COOLDOWN_TEMP = float(message['data'])
                #     elif message['type'] == 'cooldown_min':
                #         print('message[type] = cooldown_min')
                #         fuelcell.COOLDOWN_TEMP_MIN = float(message['data'])
                #     elif message['type'] == 'differential':
                #         print('message[type] = differential')
                #         fuelcell.CELL_DIFFERENTIAL_THRESHOLD = float(message['data'])

                else:
                    pru = prus[message['pruID']]
                    print(pru.id)

                    if message['action'] == "compile":
                        logging.info("COMPILE")
                        pru.compile_and_upload_program(COMPILATION_DIRECTORY,message['data']['sourceFiles'])

                    elif message['action'] == "run":
                        logging.info("RUN")
                        pru.set_freerunning_mode()
                        pru.run()

                    elif message['action'] == "reset":
                        logging.info("RESET")
                        pru.halt()
                        pru.reset()

                    elif message['action'] == "halt":
                        logging.info("HALT")
                        pru.halt()
                        #pru.reset(pru.get_program_counter_value())

                    elif message['action'] == "step":
                        logging.info("STEP")
                        pru.set_singlestep_mode()
                        pru.run()

                    elif message['action'] == "set-memory-range":
                        logging.info("SET MEMORY RANGE")
                        pru.set_memory_range(message['data']['range']['type'],message['data']['range']['offset'],message['data']['range']['addressCount'])

                    response['type'] = 'pruState'
                    response['pruState'] = pru.get_state()
                    #Queue the response
                    evt.queue.put(response)

        except Exception as e:
            #Since the closing of a websocket does not mean that the parent greenlet thread is ending, we need to kill the greenlets to avoid zombies
            monitor_prus_loop.kill()
            monitor_fuelcell_loop.kill()
            send_message_loop.kill()

            #Close the request, raise the error, and break out of the event loop
            raise e







def run(compilation_directory):
    global prus
    global fuelcell
    global COMPILATION_DIRECTORY

    COMPILATION_DIRECTORY = compilation_directory

    #Create the PRU hardware access objects
    prus = pruss.get_hash()
    fuelcell = FuelCell(debug=False)

    #Initialize and start the local webserver
    HOST = '0.0.0.0'
    PORT = 3333
    app.debug = True
    server = WSGIServer((HOST, PORT), app, handler_class=WebSocketHandler)

    try:
        print("SERVING FOREVER")
        server.serve_forever()
    finally:
        print("STOPPED SERVING FOREVER")
        #NOTE: Greenlets all run in the same thread, and therefore all are automatically killed when the app shuts down
        #Release physical memory mapped by the PRUs
        pruss.unmap_memory()
