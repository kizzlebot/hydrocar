import {Panel, Code, Editor} from './visualpru.js!jsx';
import React from 'react';
import reactDOM from 'react-dom';

var data = "var x = 5;";

const el = (
    <Panel heading={'PRU Debugger Panel'} paneltype={'success'}>
        <div className={'col-xs-12'}>
            <div className={'col-xs-6'}>
                <Editor id={'meditor'} code={data} curLine={0} readOnly={false}/>
            </div>
            <div className={'col-xs-6'}>
                <Editor id={'neditor'} code={data} curLine={0} readOnly={true}/>
            </div>
        </div>
    </Panel>
);


reactDOM.render(el, document.getElementById('mainpanel'));
