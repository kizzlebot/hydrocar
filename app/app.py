#!/usr/bin/env python

# Set this variable to "threading", "eventlet" or "gevent" to test the
# different async modes, or leave it set to None for the application to choose
# the best option based on available packages.
async_mode = None

if async_mode is None:
    try:
        import eventlet
        async_mode = 'eventlet'
    except ImportError:
        pass

    if async_mode is None:
        try:
            from gevent import monkey
            async_mode = 'gevent'
        except ImportError:
            pass

    if async_mode is None:
        async_mode = 'threading'

    print('async_mode is ' + async_mode)

# monkey patching is necessary because this application uses a background
# thread
if async_mode == 'eventlet':
    import eventlet
    eventlet.monkey_patch()
elif async_mode == 'gevent':
    from gevent import monkey
    monkey.patch_all()

import time
from threading import Thread
from flask import Flask, render_template, session, request
from flask_socketio import SocketIO, emit, join_room, leave_room, \
    close_room, rooms, disconnect
import json

app = Flask(__name__, static_url_path='/static')
app.config['SECRET_KEY'] = 'secret!'
app.jinja_env.add_extension('pyjade.ext.jinja.PyJadeExtension')

socketio = SocketIO(app, async_mode=async_mode)

thread = None





import am335x.pruss as pruss
import gevent
import evt
from FuelCell import FuelCell

def background_thread():
    """Example of how to send server generated events to clients."""

    count = 0
    while True:
        response = {}
        response['type'] = 'fuelcell'
        response['data'] = fuelcell.execute_state()

        res = {}
        res['type'] = 'fuelcell_config'
        res['data'] = fuelcell.get_config()

        socketio.emit('fuelcell', json.dumps(response))
        socketio.emit('fuelcell_config', json.dumps(res))
        time.sleep(2)


@app.route('/')
def index():
    global thread
    if thread is None:
        thread = Thread(target=background_thread)
        thread.daemon = True
        thread.start()
    return render_template('index.jade')


@socketio.on('control_params')
def handle_fuelcell(message):
    print message['type']
    if message['type'] == 'purge':
        fuelcell.PURGE_THRESHOLD = float(message['data'])
    elif message['type'] == 'delay':
        fuelcell.PURGE_DELAY = float(message['data'])
    elif message['type'] == 'cooldown':
        fuelcell.COOLDOWN_TEMP = float(message['data'])
    elif message['type'] == 'cooldown_min':
       fuelcell.COOLDOWN_TEMP_MIN = float(message['data'])
    print 'control_params'
    if message['type'] == 'purge':
        fuelcell.PURGE_THRESHOLD = float(message['data'])
    elif message['type'] == 'delay':
        fuelcell.PURGE_DELAY = float(message['data'])
    elif message['type'] == 'cooldown':
        fuelcell.COOLDOWN_TEMP = float(message['data'])
    elif message['type'] == 'cooldown_min':
       fuelcell.COOLDOWN_TEMP_MIN = float(message['data'])
    else:
        return

    res = {}
    res['type'] = 'fuelcell_config'
    res['data'] = fuelcell.get_config()
    socketio.emit('fuelcell_config', json.dumps(res))



@socketio.on('disconnect')
def test_disconnect():
    print('Client disconnected', request.sid)


def run(compilation_directory):
    global prus
    global fuelcell
    global COMPILATION_DIRECTORY

    COMPILATION_DIRECTORY = compilation_directory

    #Create the PRU hardware access objects
    prus = pruss.get_hash()
    fuelcell = FuelCell()



    #Initialize and start the local webserver
    HOST = '192.168.7.2'
    PORT = 3333

    socketio.run(app, host=HOST, port=PORT, debug=True)

if __name__ == '__main__':
    socketio.run(app, port=3333, debug=True)
