import platform
import os

if os.environ.get('MOCK'):
  print "MOCK MODE"
  from tests.bbio_mock import *
  from tests.ina219_mock import INA219
  from tests.w1_temp_mock import w1_temp
else:
  if platform.linux_distribution() != ('','','') and platform.architecture() == ('32bit', 'ELF'):
    from bbio import GPIO0_8, GPIO2_14, GPIO2_12, GPIO2_2, OUTPUT, INPUT, HIGH, LOW, AIN5, AIN3, pinMode, pinState, digitalWrite, digitalRead, delay, analogRead
    from devices.ina219 import INA219
    from devices.w1_temp import w1_temp
  else:
    from tests.bbio_mock import *
    from tests.ina219_mock import INA219
    from tests.w1_temp_mock import w1_temp


from fuelcell_state import State


class IO_Processor(object):
  def __init__(self, gpio_out_pins, gpio_in_pins, analog_in_pins, debug=False):
    self.gpio_in_pins = gpio_in_pins
    self.gpio_out_pins = gpio_out_pins
    self.analog_in_pins = analog_in_pins
    
    self.init_pins(self.gpio_out_pins, OUTPUT)
    self.init_pins(self.gpio_in_pins, INPUT)
    self.set_state('reset', State.Reset)
    self.stack = INA219(address=0x41)
    self.battery = INA219(address=0x40)
    self.previous_pin_values = {}
    
    
  def init_pins(self, pins, inOrOut):
    for k,v in pins.iteritems():
      try:
        pinMode(v, inOrOut)
      except:
        print '\tError pinMode for GPIO: %s' % k
    return

  def get_stack_voltage(self):
    self.previous_pin_values['stackVoltage'] = self.pin_values.get('stackVoltage', None)
    self.pin_values['stackVoltage'] = self.stack.getBusVoltage_V()
    return self.pin_values['stackVoltage']

  def get_battery_voltage(self):
    self.previous_pin_values['batteryVoltage'] = self.pin_values.get('batteryVoltage', None)
    self.pin_values['batteryVoltage'] = self.battery.getBusVoltage_V()
    return self.pin_values['batteryVoltage']


  def get_cell_voltage(self, cell_number):
    self.previous_pin_values['cell%d'%(cell_number+1)] = self.pin_values.get('cell%d'%(cell_number+1), None)
    self.pin_values['cell%d'%(cell_number+1)] = analogRead(self.analog_in_pins[self.analog_in_pins.keys()[cell_number]])
    return self.pin_values['cell%d'%(cell_number+1)]
    
  def get_cell1_voltage(self):
    return self.get_cell_voltage(0)
    
  def get_cell2_voltage(self):
    return self.get_cell_voltage(1)
    
  def get_cell_differential(self):
    self.previous_pin_values['cellDifferential'] = self.pin_values.get('cellDifferential', None)
    self.pin_values['cellDifferential'] = self.get_cell2_voltage() - 2 * self.get_cell1_voltage()
    return self.pin_values['cellDifferential']
  	
  def get_fuelcell_temp(self):
    self.previous_pin_values['fuelCellTemp'] = self.pin_values.get('fuelCellTemp', None)
    temp = self.w1_temp.read_temp()
    if isinstance(temp, int) or temp == 85:
      return self.pin_values['fuelCellTemp']
    self.pin_values['fuelCellTemp'] = temp
    return self.pin_values['fuelCellTemp']
    
  def set_state(self, lbl, new_state):
    self.current_state = new_state
    self.pin_values['state'] = lbl
    
  def set_control_pins(self, args):
    for k in args.keys():
      if self.pin_values.get(k, None) != args.get(k, None):
        val = HIGH if args.get(k, None) else LOW
        digitalWrite(self.gpio_out_pins[k], val)
        self.pin_values[k] = val
      
  def purge(self):
    digitalWrite(self.gpio_out_pins['purge'], HIGH)
    delay(self.PURGE_DELAY)
    digitalWrite(self.gpio_out_pins['purge'], LOW)



