from enum import Enum


class State(Enum):
  Reset = 0
  Starting = 1
  Running = 2
  Cooldown = 3
  Shutdown = 4
