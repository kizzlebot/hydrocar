from bbio import *

pins = {
    'inlet':GPIO0_8,
    'purge':GPIO2_14,
    'fan':GPIO2_12,
    'powerSourceSelect':GPIO2_2,
}


def wprint(txt):
  WARNING = '\033[93m'
  ENDC = '\033[0m'
  print WARNING + txt + ENDC 

def rprint(txt):
  FAIL = '\033[91m'
  ENDC = '\033[0m'
  print FAIL + txt + ENDC 

def gprint(txt):
  OKGREEN = '\033[92m'
  ENDC = '\033[0m'
  print OKGREEN + txt + ENDC 





def check_pinMode_set(pins, outOrIn=OUTPUT):
  gprint("Setting pinModes.")
  for k,v in pins.iteritems():
    try:
      wprint('\tpinMode(%s, %s)'%(k, outOrIn))
      pinMode(v, outOrIn)
    except:
      rprint('\tError pinMode for GPIO: %s'%k)
  gprint("Done.\n")



def check_set_output(pins, highOrLow=HIGH):
  gprint("Setting GPIO values to %s."%highOrLow)
  for k,v in pins.iteritems():
    try:
      digitalWrite(v, highOrLow)
      wprint('\tdigitalWrite(%s, %s) = %s'%(k, highOrLow, pinState(v)))
    except:
      rprint('\tError digitalWrite for GPIO: %s'%s)
  gprint("Done.\n")




if __name__ == '__main__':
  check_pinMode_set(pins, OUTPUT)
  raw_input('Enter to continue');
  check_set_output(pins, HIGH)
  raw_input('Enter to continue');
  check_set_output(pins, LOW)