import time
import random
GPIO0_8 = 'GPIO0_8'
GPIO2_14 = 'GPIO2_14'
GPIO2_12 = 'GPIO2_12'
GPIO2_2 = 'GPIO2_2'
AIN5 = 'AIN5'
AIN3 = 'AIN3'
OUTPUT = 'OUTPUT'
INPUT = 'INPUT'

HIGH = 1
LOW = 0
pin_states = {}

def pinMode(pin, inOrOut):
	return

def pinState(pin):
	return pin_states[pin]

def digitalWrite(pin, hiOrLow):
	pin_states[pin] = hiOrLow

def digitalRead(pin):
	return pin_states[pin]
def analogRead(pin):
	return random.random()
def delay(ms):
	time.sleep(float(ms)/1000)