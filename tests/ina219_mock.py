from __future__ import division
import time
import random
import math

from itertools import izip
# ===========================================================================
# INA219 Class
# ===========================================================================

class INA219:
	# Constructor
	def __init__(self, address=0x40, debug=False):
		self.address = address
		self.ina219SetCalibration_32V_2A()
		self.previousVoltage = []
		self.previousCurrent = []

	def twosToInt(self, val, len):
		# Convert twos compliment to integer
		if(val & (1 << len - 1)):
			val = val - (1<<len)
		return val

	def ina219SetCalibration_32V_2A(self):
		return


	def pairwise(iterable):
		a = iter(iterable)
		return izip(a, a)

	def differentiate(num_list):
		x = range(0, len(num_list))
		rtn = []
		for ((a, b), (c, d)) in zip(pairwise(x), pairwise(num_list)):
			rtn.append( (d - c) / (b - a) )
		return

	def getBusVoltage_raw(self):
		return random.randint(2,10) + random.random()
		
	def getShuntVoltage_raw(self):
		return random.randint(2,10) + random.random()

	def getCurrent_raw(self):
		return random.randint(2,10) + random.random()

	def getPower_raw(self):
		return random.randint(2,10) + random.random()

	def getShuntVoltage_mV(self):
		return random.randint(2,10) + random.random()
		
	def getBusVoltage_V(self):
		return random.randint(2,10) + random.random()
		
	def getCurrent_mA(self):
		return random.randint(2,10) + random.random()
		
	def getPower_mW(self):
		return random.randint(2,10) + random.random()
