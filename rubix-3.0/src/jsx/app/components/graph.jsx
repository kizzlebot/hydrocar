import React from 'react';
// import ChartContainer from 'common/chartContainer';
var _ = require('underscore');
var $ = require('jquery');




class ChartContainer extends React.Component {
  render() {
    return (
        <div id={this.props.id + '-container'}>
          <div {...this.props}></div>
        </div>
    );
  }
}



var Graph = React.createClass({
  getInitialState:function(props) {
    return { chart:null, legend: '', nextLabel:1, keys:[], lastUpdate: Math.floor(Date.now() / 1000), data:{ labels: [], datasets: [] } };
  },
  componentDidMount: function() {
    this.state.nextLabel = 0;
    ReactBootstrap.Dispatcher.on('fuelcell', this.updateData.bind(this));
    this.setState(this.state);
  },
  updateData: function(data){
    var curTime = (Math.floor(Date.now() / 1000));
    if ((curTime - this.state.lastUpdate) < 2) return;
    else this.state.lastUpdate = curTime;


    var newPoints = [];
    var dataKeys = Object.keys(data);      

    this.props.keys.forEach((el, i) => {
      if (dataKeys.indexOf(el) != -1){
        var datasetIndex = _.findIndex(this.state.data.datasets, (d) => d.label == el);
        if (datasetIndex != -1) this.state.data.datasets[datasetIndex].data.push(data[el]);
        else this.state.data.datasets.push(this.newDataSet(el, data[el]));
        newPoints.push(data[el]);
      }
    });

    
    if (!this.state.chart) {
      $(`#${this.props.id}`).replaceWith(`<canvas id="${this.props.id}" width='${this.props.width} height='${this.props.height}'></canvas>`);
      this.state.ctx = $(`#${this.props.id}`).get(0).getContext("2d");
      this.state.chart = new Chart(this.state.ctx).Line(this.state.data);
    }
    this.state.chart.addData(newPoints, this.state.nextLabel);
    var nextLabel = this.state.chart.scale.xLabels[this.state.chart.scale.xLabels.length-1]+1;


    if (this.state.data.datasets.length > 0 && this.state.data.datasets[0].data.length > 10) {
      this.state.chart.removeData();
      this.state.data.datasets.forEach((d, i) => {
        this.state.data.datasets[i].data.splice(0,1);
      });
    }
    if (this.state.nextLabel%10 == 0){
      $(`#${this.props.id}-container`).empty();
      $(`#${this.props.id}-container`).append(`<canvas id='${this.props.id}' width='${this.props.width} height='${this.props.height}'></canvas>`);
      this.state.ctx = $(`#${this.props.id}`).get(0).getContext("2d");
      this.state.chart = new Chart(this.state.ctx).Line(this.state.data);
    }

    this.state.nextLabel = nextLabel;
    this.setState(this.state);
  },
  newDataSet: function(label, initVal){
    return {
      label: label,
      fillColor: `rgba(${Math.floor(Math.random()*1000)%256},${Math.floor(Math.random()*1000)%256},${Math.floor(Math.random()*1000)%256},0.2)`,
      strokeColor: "rgba(220,220,220,1)",
      pointColor: "rgba(220,220,220,1)",
      pointStrokeColor: "#fff",
      pointHighlightFill: "#fff",
      pointHighlightStroke: "rgba(220,220,220,1)",
      data: [initVal]
    }
  },
  componentWillUnmount:function() {
    this.state.chart.destroy();
  },
  render: function() {
    return (
        <ChartContainer id={this.props.id} height={this.props.width} width={this.props.width} name={this.props.name} />
    );
  }
});



export default Graph;
