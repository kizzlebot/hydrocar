import classNames from 'classnames';
import SidebarMixin from 'global/jsx/sidebar_component';

import Header from 'common/header';
import Sidebar from 'common/sidebar';
import Footer from 'common/footer';
var _ = require('underscore');

var Body = React.createClass({
  statics: {
    counter: 0,
    getCounter: () => 'counter-' + ++Body.counter
  },
  getInitialState: function() {
    return {
      mode: 'popup',
      refresh: Body.getCounter(), // used to redraw the component
      serverData:{},
      clientData:{
        purge:{
          label:'Purge Voltage',
          options:this.getEditableConfig(3, 10, 0.5),
          initialSelection:6
        },
        delay:{
          label:'IO Delay',
          options:this.getEditableConfig(100, 10000, 50),
          initialSelection:15
        },
        cooldown:{
          label:'Overheat Temperature',
          options:this.getEditableConfig(20, 70, 2),
          initialSelection:12
        },
        cooldown_min:{
          label:'Cooldown Temp. Min',
          options:this.getEditableConfig(20, 70, 2),
          initialSelection:8
        }
      }
    };
  },
  getEditableConfig(range_min, range_max, increments){
    return {
      mode: 'popup',
      source: _.range(range_min, range_max, increments || 1).map((v, i) => {
        return {value:i, text:v};
      }),
      display: function(value, sourceData) {

        var colors = {'': 'gray', 1: 'green', 2: 'blue'};
        var elem = $.grep(sourceData, function(o){return o.value == value;});

        if(elem.length) $(this).text(elem[0].text).css('color', colors[value]);
        else $(this).empty();    
      }
    };
  },
  renderEditable: function() {
    $('.xeditable').editable({mode: 'inline'});

    for ( var k in this.state.clientData){
      $(`#${k}`).editable(this.state.clientData[k].options);
    }
    
    var self = this;
    $('#user .editable').on('hidden', function(e, reason){
      if(reason === 'save' || reason === 'nochange') {
        console.log('fuelcell_emit');
        ReactBootstrap.Dispatcher.emit('send', {action:'control_params',type:this.id, data:parseFloat(this.innerText)});
        var $next = $(this).closest('tr').next().find('.editable');
        $next.focus();
      }
    });
  },
  componentDidMount: function() {
    this.renderEditable();
    ReactBootstrap.Dispatcher.on('fuelcell_config', this.updateData.bind(this));
  },
  updateData: function(key){
    var self = this;
    if (Object.keys(this.state.serverData).length == 0){
      Object.keys(key).map((k) => {
        if (self.state.clientData[k]){
          var i = _.findIndex(self.state.clientData[k].options.source, (d) => d.text == key[k]);
          self.state.clientData[k].initialSelection = (i > -1) ? self.state.clientData[k].options.source[i].value : self.state.clientData[k].initialSelection ;
        }
      });
    }
    this.state.serverData = key;
    this.setState(this.state);
  },
  render: function() {
    return (      
      <Table striped bordered id='user' style={{margin: 0}}>
        <thead>
          <tr>
            <th>Parameter</th>
            <th>Client</th>
            <th>Server</th>
          </tr>
        </thead>
        
        <tbody>
          {Object.keys(this.state.clientData).map((k) => {
            return (
              <tr>
                <td>{this.state.clientData[k].label}</td>
                <td><a href='#' key={this.state.refresh} id={k} data-type='select' data-pk='1' data-title={this.state.clientData[k].label} data-value={this.state.clientData[k].initialSelection} ></a></td>
                <td>{this.state.serverData[k]|| 'N/A'}</td>
              </tr>    
            );
          })}          
        </tbody>
      </Table>
    );
  }
});




export default class extends React.Component {
  render() {
    return (<Body />);
  }
}
