import classNames from 'classnames';
import SidebarMixin from 'global/jsx/sidebar_component';

import Header from 'common/header';
import Sidebar from 'common/sidebar';
import Footer from 'common/footer';
var _ = require('underscore');

var Body = React.createClass({
  statics: {
    counter: 0
  },
  getInitialState: function() {
    return { data:{}, previousState: null }
  },
  componentDidMount: function() {
    ReactBootstrap.Dispatcher.on('fuelcell', this.updateData.bind(this));
    Messenger.options = {theme: 'flat'};
  },
  updateData: function(key){
    if (key.hasOwnProperty('state') && key['state'] != this.state.data['state']){
      this.state.previousState = this.state.data['state'];
      Messenger().post(`Fuel Cell State: ${key['state']}`);
    }
    this.state.data = key;
    this.setState(this.state);
  },
  formatHeading:function(h){
    // Camelcase to regular text
    return (typeof h == 'string') ? h.replace(/([A-Z])/g, ' $1').replace(/^./, (str) =>  str.toUpperCase()) : 'N/A';
  },
  render: function() {
    return (
      <Table striped bordered id='status' style={{margin: 0}}>
        <thead>
          <tr>
            <th>Status</th>
            <th>Value</th>
          </tr>
        </thead>
        <tbody>
          {Object.keys(this.state.data).map((e) => {
            return (
              <tr>
                <td>{this.formatHeading(e)}</td>
                <td>{this.state.data[e]}</td>
              </tr>);
          })}
        </tbody>
      </Table>
    );
  }
});

export default class extends React.Component {
  render() {
    return (<Body />);
  }
}
