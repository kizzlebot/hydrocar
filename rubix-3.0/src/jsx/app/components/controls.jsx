export default class extends React.Component {
  componentDidMount() {
    var editor = global.CodeMirror.fromTextArea($('#code').get(0), {
      lineNumbers: true,
      styleActiveLine: true,
      matchBrackets: true,
      theme: 'ambiance'
    });
    var compiled = global.CodeMirror.fromTextArea($('#compiled').get(0), {
      lineNumbers: true,
      styleActiveLine: true,
      matchBrackets: true,
      theme: 'ambiance',
      readOnly:true
    });
  }
  render() {
    return (
      <Col sm={this.props.sm||12}>
        <PanelContainer controlStyles='bg-blue fg-white'>
          <PanelHeader className='bg-blue fg-white' style={{margin: 0}}>
            <Grid>
              <Row>
                <Col xs={12}>
                  <h3>PRU</h3>
                </Col>
              </Row>
            </Grid>
          </PanelHeader>
          <PanelBody>
            <Grid>
              <Row style={{padding: 25}}>
                <Col sm={12}>
                  <p>
                    <Button outlined style={{marginBottom: 5}} bsStyle='success'>Run</Button>{' '}
                    <Button outlined style={{marginBottom: 5}} bsStyle='danger'>Halt</Button>{' '}
                  </p>
                  <hr/>
                </Col>
              </Row>
              <Row>
                <Col xs={6} >
                  <Textarea id='code' name='code'></Textarea>
                </Col>
                <Col xs={6} >
                  <Textarea id='compiled' name='code'></Textarea>
                </Col>
              </Row>
            </Grid>
          </PanelBody>
        </PanelContainer>
      </Col>
    );
  }
}
