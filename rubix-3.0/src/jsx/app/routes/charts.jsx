import classNames from 'classnames';
import SidebarMixin from 'global/jsx/sidebar_component';
import Header from 'common/header';
import Sidebar from 'common/sidebar';
import Footer from 'common/footer';

import Graph from 'components/graph';
import Controls from 'components/controls';
import ParamPanel from 'components/parameter_panel';

import StatusTbl from 'components/status_tbl';


var SOCKET_ADDRESS = __SOCKETADDRESS__;
require('expose?io!socket.io-client');



function WebsocketMixin(ComposedComponent) {
  return React.createClass({
    displayName: 'WebsocketMixin',
    getInitialState:function() { return { ws:null }; },

    componentDidMount:function() {
      var self = this;

      // self.ws = new window.WebSocket(SOCKET_ADDRESS);

      // self.ws.onopen = () => {
      //   self.ws.send(JSON.stringify({ action: 'connect'})) 
      // }

      // self.ws.onmessage = (evt) => {
      //   var data = JSON.parse(evt.data);
      //   console.log('onmessage');
      //   if (data.type){
      //     ReactBootstrap.Dispatcher.emit(data.type, data.data)  
      //   }
      // };

      /**
      * Remote Procedure Calls
      */ 
      ReactBootstrap.Dispatcher.on('send', function(d){
        // var d = JSON.parse(d);
        if (d.hasOwnProperty('action')) self.socket.emit(d.action, d);
        else if (d.hasOwnProperty('type')) self.socket.emit(d.type, d);
      });  
      

      ReactBootstrap.Dispatcher.on('fuelcell_emit', function(message){
        self.ws.send(JSON.stringify(message));
      });


      self.socket = io();
      self.socket.on('message', function(msg){
        console.log('message');
        JSON.parse(msg.data); 
      });

      self.socket.on('fuelcell', function(msg){
        // console.log('fuelcell');
        var data = JSON.parse(msg); 
        ReactBootstrap.Dispatcher.emit(data.type, data.data);  
      });

      self.socket.on('fuelcell_config', function(msg){
        console.log('fuelcell_config');
        var data = JSON.parse(msg); 
        ReactBootstrap.Dispatcher.emit(data.type, data.data);  
      });

      self.socket.on('pruState', function(msg){
        // console.log('pruState');
        // console.log(msg);
      });

    },
    render:function() {
      var panelColors = ['bg-brown50 fg-white', 'bg-green fg-white', 'bg-darkgreen45 fg-white', 'bg-lightred fg-white', 'bg-blue fg-white', 'bg-purple fg-white'];
      return (
        <Col xs={this.props.xs || 6} sm={this.props.sm || 6}>
          <PanelContainer>
            <Panel>
              <PanelHeader className={panelColors[Math.floor(Math.random()*100) % panelColors.length]}>
                <Grid>
                  <Row>
                    <Col xs={12}>
                      <h3>{this.props.name}</h3>
                    </Col>
                  </Row>
                </Grid>
              </PanelHeader>
              <PanelBody style={{padding: 25}} className='text-center'>
                <ComposedComponent {...this.props}/>
              </PanelBody>
            </Panel>
          </PanelContainer>
        </Col>
      );
      
    }
  });
}



@SidebarMixin
export default class extends React.Component {
  render() {
    var Temp = WebsocketMixin(Graph);
    var Volt = WebsocketMixin(Graph);
    var Param = WebsocketMixin(ParamPanel);

    var Status = WebsocketMixin(StatusTbl);
    var classes = classNames({'container-open': this.props.open});
    return (
      <Container id='container' className={classes}>
        <Sidebar />
        <Header />
        <Container id='body'>
          <Grid>
            <Row>
              <Temp id={'temperature-chart'} keys={['fuelCellTemp', 'cpuTemp']}         sm={6} name={'Temperature Chart'}/>
              <Volt id={'voltage-chart'}     keys={['stackVoltage', 'batteryVoltage','cell1', 'cell2']}  sm={6} name={'Voltage Chart'}/>
            </Row>
            <Row>
              <Param id={'control_params'} sm={6} name={'Control Parameters'}/>
              <Status id={'status'} sm={6} name={'Status'}/>
            </Row>
          </Grid>
        </Container>
        <Footer />
      </Container>
    );
  }
}
