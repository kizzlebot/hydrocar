import { Router } from 'react-router';
import Location from 'react-router/lib/Location';

if(window.hasOwnProperty('vex')) {
  vex.defaultOptions.className = 'vex-theme-flat-attack';
}

var setWindowListener = () => {
  var hidden = "hidden";

  function onchange (evt) {
    var v = "visible", h = "hidden";
    var evtMap = {focus:v, focusin:v, pageshow:v, blur:h, focusout:h, pagehide:h};

    evt = evt || window.event;
    var windowEvent = (evt.type in evtMap) ? evtMap[evt.type] : (this[hidden] ? "hidden" : "visible");
    ReactBootstrap.Dispatcher.emit('windowEvent', windowEvent);
    console.log(windowEvent);
  }


  if (hidden in document) document.addEventListener("visibilitychange", onchange);
  else if ((hidden = "mozHidden") in document) document.addEventListener("mozvisibilitychange", onchange);
  else if ((hidden = "webkitHidden") in document) document.addEventListener("webkitvisibilitychange", onchange);
  else if ((hidden = "msHidden") in document) document.addEventListener("msvisibilitychange", onchange);
  // IE 9 and lower:
  else if ("onfocusin" in document) document.onfocusin = document.onfocusout = onchange;
  // All others:
  else window.onpageshow = window.onpagehide = window.onfocus = window.onblur = onchange;

  if( document[hidden] !== undefined ) onchange({type: document[hidden] ? "blur" : "focus"});
}



var onUpdate = (notReady) => {
  // cleanup (do not modify)
  rubix_bootstrap.core.reset_globals_BANG_();
  if(window.Rubix) window.Rubix.Cleanup();

  Pace.restart();
  if(window.hasOwnProperty('ga') && typeof window.ga === 'function') {
    window.ga('send', 'pageview', {
     'page': window.location.pathname + window.location.search  + window.location.hash
    });
  }

  if(!notReady) {
    // l20n initialized only after everything is rendered/updated
    l20n.ready();
    setTimeout(() => {
      $('body').removeClass('fade-out');
    }, 500);
  }
};

var InitializeRouter = (routes) => {
  onUpdate(true);
  var rootInstance = React.render(routes, document.getElementById('app-container'), () => {
    // l20n initialized only after everything is rendered/updated
    l20n.ready();
    setTimeout(() => {
      $('body').removeClass('fade-out');
    }, 500);
  });

  // initialize react-hot-loader
  if(module.hot) {
    require('react-hot-loader/Injection').RootInstanceProvider.injectProvider({
      getRootInstances() {
        // Help React Hot Loader figure out the root component instances on the page:
        return [rootInstance];
      }
    });
  }
};

module.exports = (routes) => {
  if('document' in window) {
    setWindowListener();
    InitializeRouter(routes(true, onUpdate));
  } 
  else {
    // called only server side!
    if(__BACKEND__ === 'rails') {
      global.StaticComponent = React.createClass({
        render() {
          var Handler = null;
          var location = new Location(this.props.path, this.props.query);
          ReactBootstrap.Dispatcher.removeAllListeners();
          rubix_bootstrap.core.reset_globals_BANG_();
          Router.run(routes(false), location, (e, i, t) => {
            Handler = <Router {...i} />;
          });
          return Handler;
        }
      });
    } else {
      return () => {
        ReactBootstrap.Dispatcher.removeAllListeners();
        rubix_bootstrap.core.reset_globals_BANG_();
        return routes(false);
      };
    }
  }
};
