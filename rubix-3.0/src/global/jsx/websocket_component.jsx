import React from 'react';
var SOCKET_ADDRESS = "ws://localhost:3333/";



var WebsocketMixin = function(ComposedComponent) {
  return React.createClass({
    displayName: 'WebsocketMixin',
    getInitialState:function() {
      return { ws:null };
    },
    componentDidMount:function() {
      var self = this;
      self.ws = new window.WebSocket(SOCKET_ADDRESS);

      this.ws.onopen = function(){
        console.log('WebsocketMixin:Opened');
        self.ws.send(JSON.stringify({action:'connect'}));
      }

      this.ws.onmessage = function(evt){
        var data = JSON.parse(evt.data);
        if (data instanceof Array){
          ReactBootstrap.Dispatcher.emit(data.type, data.data);  
        }
        else if (data instanceof Object){
          var ob = {};
          ReactBootstrap.Dispatcher.emit('data', data.data); 
        }
      }
    },
    render:function() {
      return <ComposedComponent {...this.props} />;
    }
  });
}

export default WebsocketMixin;